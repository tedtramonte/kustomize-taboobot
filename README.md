# Kustomize - TabooBot
This repository deploys [TabooBot](https://gitlab.com/tedtramonte/taboobot) to a Kubernetes cluster. It is currently intended to be deployed by ArgoCD using Kustomize but can be deployed by hand if necessary.

For information on the application itself, visit [its repository](https://gitlab.com/tedtramonte/taboobot).
